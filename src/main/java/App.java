import org.apache.pdfbox.io.IOUtils;

import java.io.IOException;
import java.util.Base64;
import java.util.Objects;

public class App {

    public static void main(String[] args) throws IOException {
        final byte[] frontImageBytes = IOUtils.toByteArray(
                    Objects.requireNonNull(App.class.getClassLoader().getResourceAsStream("OP1.png")));
        final String frontImageEncoded = Base64.getEncoder().encodeToString(frontImageBytes);

        final byte[] backImageBytes = IOUtils.toByteArray(
                    Objects.requireNonNull(App.class.getClassLoader().getResourceAsStream("OP2.png")));
        final String backImageEncoded = Base64.getEncoder().encodeToString(backImageBytes);

        final PdfImageService service = new PdfImageService();

        final String base64Pdf = service.createPdfLandscape(frontImageEncoded, backImageEncoded);

        System.out.println(base64Pdf);

    }

}
