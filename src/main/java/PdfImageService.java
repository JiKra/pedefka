import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.vandeseer.easytable.TableDrawer;
import org.vandeseer.easytable.structure.Row;
import org.vandeseer.easytable.structure.Table;
import org.vandeseer.easytable.structure.cell.ImageCell;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Base64;

import static java.awt.Color.DARK_GRAY;
import static java.awt.Color.WHITE;
import static org.apache.pdfbox.pdmodel.font.PDType1Font.HELVETICA;
import static org.vandeseer.easytable.settings.HorizontalAlignment.CENTER;
import static org.vandeseer.easytable.settings.VerticalAlignment.MIDDLE;

public class PdfImageService {

    private static final float PADDING = 50f;

    private static final float LANDSCAPE_POINTS_PER_INCH = 72;
    private static final float LANDSCAPE_POINTS_PER_MM = 1 / (10 * 2.54f) * LANDSCAPE_POINTS_PER_INCH;

    public String createPdfPortrait(
                final String base64EncodedFront,
                final String base64EncodedBack) throws IOException {

        final PDPage page = new PDPage(PDRectangle.A4);

        return createPdf(base64EncodedFront, base64EncodedBack, page);
    }

    public String createPdfLandscape(
                final String base64EncodedFront,
                final String base64EncodedBack) throws IOException {

        final PDPage page = new PDPage(new PDRectangle(297 * LANDSCAPE_POINTS_PER_MM, 210 * LANDSCAPE_POINTS_PER_MM));

        return createPdf(base64EncodedFront, base64EncodedBack, page);
    }

    private String createPdf(String base64EncodedFront, String base64EncodedBack, PDPage page) throws IOException {
        final PDDocument document = new PDDocument();
        document.addPage(page);

        float startY = page.getMediaBox().getHeight() - PADDING;

        final Table table = createTable(document, base64EncodedFront, base64EncodedBack);

        try (final PDPageContentStream contentStream = new PDPageContentStream(document, page)) {
            TableDrawer.builder()
                        .page(page)
                        .contentStream(contentStream)
                        .table(table)
                        .startX(PADDING)
                        .startY(startY)
                        .endY(PADDING)
                        .build()
                        .draw(() -> document, () -> new PDPage(PDRectangle.A4), PADDING);
        }

        // TODO: 06.05.20  Delete this line!
        document.save("op.pdf");

        final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        document.save(outputStream);
        document.close();

        return Base64.getEncoder().encodeToString(outputStream.toByteArray());
    }

    private Table createTable(
                final PDDocument document,
                final String base64EncodedFront,
                final String base64EncodedBack) throws IOException {

        final ImageCell front = ImageCell.builder()
                    .verticalAlignment(MIDDLE)
                    .horizontalAlignment(CENTER)
                    .borderWidth(1)
                    .image(createImage(document, base64EncodedFront, "front"))
                    .scale(0.4f)
                    .build();

        final ImageCell back = ImageCell.builder()
                    .verticalAlignment(MIDDLE)
                    .horizontalAlignment(CENTER)
                    .borderWidth(1)
                    .image(createImage(document, base64EncodedBack, "back"))
                    .scale(0.4f)
                    .build();

        final Row imagesRow = Row.builder()
                    .add(front)
                    .add(back)
                    .build();

        return Table.builder()
                    .addColumnsOfWidth(300, 300)
                    .borderColor(WHITE)
                    .textColor(DARK_GRAY)
                    .fontSize(7)
                    .font(HELVETICA)
                    .addRow(imagesRow)
                    .build();
    }

    private PDImageXObject createImage(
                final PDDocument document,
                final String base64EncodedImage,
                final String name) throws IOException {

        byte[] imageBytes = Base64.getDecoder().decode(base64EncodedImage);
        return PDImageXObject.createFromByteArray(document, imageBytes, name);
    }
}
